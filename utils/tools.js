// Returns The Current timestamp with One hour subtracted from it
function currentTimestampMinusOneHour () {
	let dt = new Date()
	dt.setHours( dt.getHours() - 1 )
	return dt.getTime()
}


function LTEoneHour(timestamp) {
	return new Date(timestamp).getTime() >= currentTimestampMinusOneHour()
}

// Formular for finding distance between 2-points
function distanceBetweenTwoPoint (p2, p1 ) {
	return Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2))
}


function random(len, min, max) {
	if (len) {
		var max = Math.pow(10, (len+1)) - 1
		var min = Math.pow(10, (len))
	}
	
	return Math.floor(Math.random() * (max - min + 1) ) + min;
}
module.exports = {
	LTEoneHour,
	random,
	currentTimestampMinusOneHour,
	distanceBetweenTwoPoint
}