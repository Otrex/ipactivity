const { RequestError } = require ("../services/Exceptions")

exports.check = (req, res, next) => {
	if (req.body){
		if (!req.body.coordinates) throw new RequestError("No Coordinates Entered")
		if (!req.body.coordinates.x && !req.body.coordinates.y ) throw new RequestError("Please Enter the coordinates as x:value, y:value")
	}
	next()
}