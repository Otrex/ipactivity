const IPActivity = require('../models/').IPActivity
const catchAsync = require('../services/core/CatchAsync')
const {RequestError} = require('../services/Exceptions')
const ipServices = require('../services/IpServices')


exports.store = catchAsync(async(req, res, next) => {
	let { ip , coordinates } = req.body
	const [ ipdata , created ] = await IPActivity.findOrCreate({'where' : {ip}})
	await ipServices.UpdateCoordinate(ipdata, coordinates)
	res.status(200).json({msg : "ok"})
})


exports.get = catchAsync(async(req,res, next) => {
	let { ip } = req.query
	let ipdata = await IPActivity.findOne({'where' : {ip}})
	if (!ipdata) throw new RequestError('We dont have this IP ADDRESS')
	let coordinates = await ipdata.getCoordinates()
	let result = ipServices.getDistance(coordinates)
	res.status(200).json({result})
})

exports.getAll = catchAsync(async(req, res, next)=> {
	let all = await IPActivity.findAll({'include' : 'Coordinates'})
	res.status(200).json(all)
})