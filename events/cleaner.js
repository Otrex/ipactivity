var events = require('events');
const { Op } = require('sequelize')
const Coordinate = require('../models').Coordinate
var eventEmitter = new events.EventEmitter();

//Create an event handler to delete old records
var deleteEvent = async function () {
  await Coordinate.destroy({ 'where' : {
  	createdAt: {
        [Op.lt]: new Date(new Date() - 1 * 60 * 60 * 1000)
    }
  }})
}

//Assign the event handler to an event:
eventEmitter.on('clear-expired-coordinates', deleteEvent);


module.exports = eventEmitter